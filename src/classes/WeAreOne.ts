import fetch, {Response} from 'node-fetch';
import * as utils from '../utils';
import {Client, Collection, Guild, GuildMember, Role, Snowflake} from "discord.js";

interface Show {
  n: string,
  m: string,
  mi: number,
  ss: string,
  s: number,
  e: number
}

interface Station {
  shortName: string,
  notation: string,
  domain: string
}

interface Stations {
  [index: string]: Station
}

interface WaoOnairDjs {
  [index: string]: {
    show: Show
    stream: Station
    streamId: string
  }
}

export class WeAreOne {
  private readonly client: Client;
  private readonly config: Config;
  private readonly dateTimeFormat: Intl.DateTimeFormat;
  private guild: Guild;
  private readonly interval: number = 60 * 1000; // 1 minute
  private onairRole: Role;
  private waoStreams: Stations;

  constructor(client: Client, config: Config) {
    this.client = client;
    this.config = config;
    this.dateTimeFormat = new Intl.DateTimeFormat(
        'de-DE',
        {
          dateStyle: 'full',
          timeStyle: 'short',
          timeZone: 'Europe/Amsterdam'
        } as Intl.DateTimeFormatOptions
    );
  }

  init = async () => {
    if (false === !!Object.assign({}, this.config.weareone).enabled) {
      return;
    }

    this.guild = await utils.findGuild(this.client, this.config.guild_id);
    this.onairRole = await utils.findRole(this.guild, this.config.weareone.onair_role_id);

    const stations: Response = await fetch('https://api.tb-group.fm/v1/stations');
    this.waoStreams = await stations.json();

    this.client.setInterval(this.callback, this.interval);
    console.log(`registered task.weareone with ${this.interval}ms interval`);

    await this.callback();
  };

  callback = async () => {
    try {
      const dateTimestamp = (new Date()).getTime();
      const waoOnairDjs: WaoOnairDjs = {};

      // go through all shows in the schedule
      for (const [streamId, stream] of Object.entries(this.waoStreams)) {
        const result = await fetch(`https://api.tb-group.fm/v1/showplan/${streamId}`);
        const json = await result.json();

        // check each show in the schedule
        for (const show of json) {
          // remember the moderator that is currently onair and for which stream
          if (show.s <= dateTimestamp && dateTimestamp <= show.e) {
            if (this.config.weareone.static_mapping) {
              waoOnairDjs[show.mi] = {
                show: show,
                stream: stream,
                streamId: streamId,
              };
            } else if(this.config.weareone.dynamic_mapping) {
              waoOnairDjs[show.m] = {
                show: show,
                stream: stream,
                streamId: streamId,
              };
            }
          }
        }
      }

      if (this.config.weareone.static_mapping) {
        // going through all registered DJs
        for (const dj of this.config.weareone.static_mapping.djs) {
          const member: GuildMember = await this.guild.members.fetch(dj.discordId);
          const identifier: string = dj.waoId;

          await this.handleOnairOffair(waoOnairDjs, identifier, member, dj.announce);
        }
      } else if (this.config.weareone.dynamic_mapping) {
        const djGroupRole: Role = await utils.findRole(this.guild, this.config.weareone.dynamic_mapping.dj_group_id);

        for (const member of djGroupRole.members.values()) {
          const identifier: string = member.displayName;

          await this.handleOnairOffair(waoOnairDjs, identifier, member, true);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  private handleOnairOffair = async (waoOnairDjs: WaoOnairDjs, identifier: string, member: GuildMember, announce: boolean) => {
    if (waoOnairDjs[identifier]) { // DJ should be onair
      // member doesn't have the onair role => setting onair
      if (!utils.hasRole(member, this.onairRole)) {
        if (!this.waoStreams[waoOnairDjs[identifier].streamId]) {
          return;
        }

        const show = waoOnairDjs[identifier].show;
        const stream = waoOnairDjs[identifier].stream;
        const endTime = new Date(show.e);
        const streamOnairRole: Role = await utils.findRoleByName(this.guild, `onair-${stream.domain}`);

        console.log(`task.weareone: setting ${member.displayName} onair`);

        await member.roles.add(this.onairRole);

        if (!!streamOnairRole) {
          await member.roles.add(streamOnairRole);
        }

        if (true === announce) {
          await utils.writeOnairMessage(
              this.client,
              this.config.guild_id,
              this.config.weareone.announce_channels,
              `${member} ist jetzt bis ${this.dateTimeFormat.format(endTime)} auf dem **WeAreOne ${stream.notation}** Stream mit der Show __${show.n}__ onair: https://www.${stream.domain}/webplayer`
          );
        }
      }
    } else { // DJ should be offair
      // member still has onair role => setting offair
      if (utils.hasRole(member, this.onairRole)) {
        const streamOnairRoles: Collection<Snowflake, Role> = await utils.findRolesByNamePrefix(this.guild, 'onair-');

        console.log(`task.weareone: setting ${member.displayName} offair`);

        await member.roles.remove(this.onairRole);

        if (!!streamOnairRoles) {
          for (const streamOnairRole of streamOnairRoles.values()) {
            await member.roles.remove(streamOnairRole);
          }
        }

        await utils.writeDM(member, 'Ich habe dich bei **We aRe oNe** offair genommen.');
      }
    }
  };
}
