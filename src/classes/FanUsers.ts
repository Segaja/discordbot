import { EventSubscriber } from './EventSubscriber';
import * as utils from '../utils';
import {Client, GuildMember, Role} from "discord.js";

export class FanUsers extends EventSubscriber {
  constructor(client: Client, config: Config) {
    super(client, config);

    if (false === !!Object.assign({}, config.fan_users).enabled) {
      return;
    }

    this.eventListeners = {
      guildMemberAdd:    this.guildMemberAddEventListener,
      guildMemberUpdate: this.guildMemberUpdateEventListener,
      ready:             this.readyEventListener
    };
  };

  guildMemberAddEventListener = async (member: GuildMember) => {
    try {
      const fanRole: Role = await utils.findRole(await utils.findGuild(this.client, this.config.guild_id), this.config.fan_users.role_id);

      if (member.displayName.startsWith(this.config.fan_users.prefix) && !utils.hasRole(member, fanRole)) {
        await member.roles.add(fanRole);
      }
    } catch (error) {
      console.error(error);
    }
  };

  guildMemberUpdateEventListener = async (oldMember: GuildMember, newMember: GuildMember) => {
    try {
      const fanRole: Role = await utils.findRole(await utils.findGuild(this.client, this.config.guild_id), this.config.fan_users.role_id);

      if (oldMember.displayName !== newMember.displayName) {
        if (newMember.displayName.startsWith(this.config.fan_users.prefix)) {
          await newMember.roles.add(fanRole);
        } else if (!newMember.displayName.startsWith(this.config.fan_users.prefix)) {
          await newMember.roles.remove(fanRole);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  readyEventListener = async () => {
    try {
      for (const [memberId, member] of Object.entries((await utils.findGuild(this.client, this.config.guild_id)).members.fetch())) {
        if (member.displayName) {
          await this.guildMemberAddEventListener(member);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };
}
