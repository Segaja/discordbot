import * as configJson from '../../config.json'

export class ConfigLoader {
  private readonly config: Config;

  constructor() {
    try {
      this.config = configJson;

      this.fanUsers();
      this.guildSettings();
      this.rautemusik();
      this.twitch();
      this.weareone();

      console.log('ConfigValidator: is valid');
    } catch (error) {
      console.error(`ConfigValidator: ${error}`);
      process.exit(1);
    }
  };

  get = (): Config => {
    return this.config;
  }

  fanUsers = () => {
    if (this.config.fan_users && this.config.fan_users.enabled) {
      if (!this.config.fan_users.prefix || this.config.fan_users.prefix.length === 0) {
        throw new Error('Please provide a "fan_users.prefix" that is not empty');
      }

      if (!this.config.fan_users.role_id) {
        throw new Error('Please provide a "fan_users.roleId"');
      }
    }
  };

  guildSettings = () => {
    if (!this.config.guild_id) {
      throw new Error('Please provide a "guild_id"');
    }
  };

  rautemusik = () => {
    if (this.config.rautemusik && this.config.rautemusik.enabled) {
      if (!this.config.rautemusik.announce_channels || !Array.isArray(this.config.rautemusik.announce_channels)) {
        throw new Error('Please provide an array of "rautemusik.announce_channels"');
      }

      if (!this.config.rautemusik.djs || typeof this.config.rautemusik.djs !== 'object') {
        throw new Error('Please provide an object of "rautemusik.djs"');
      }

      if (!this.config.rautemusik.onair_role_id) {
        throw new Error('Please provide a "rautemusik.onair_role_id"');
      }

      if (!this.config.rautemusik.streams || !Array.isArray(this.config.rautemusik.streams)) {
        throw new Error('Please provide an array of "rautemusik.streams"');
      }
    }
  };

  twitch = () => {
    if (this.config.twitch && this.config.twitch.enabled) {
      if (!this.config.twitch.announce_channels || !Array.isArray(this.config.twitch.announce_channels)) {
        throw new Error('Please provide an array of "twitch.announce_channels"');
      }

      if (!this.config.twitch.onair_role_id) {
        throw new Error('Please provide a "twitch.onair_role_id"');
      }

      if (!this.config.twitch.team_role_id) {
        throw new Error('Please provide a "twitch.team_role_id"');
      }
    }
  };

  weareone = () => {
    if (this.config.weareone && this.config.weareone.enabled) {
      if (!this.config.weareone.announce_channels || !Array.isArray(this.config.weareone.announce_channels)) {
        throw new Error('Please provide an array of "weareone.announce_channels"');
      }

      if (!this.config.weareone.static_mapping && !this.config.weareone.dynamic_mapping) {
        throw new Error('Please provide either "weareone.static_mapping" or "weareone.dynamic_mapping"');
      }

      if (!this.config.weareone.onair_role_id) {
        throw new Error('Please provide a "weareone.onair_role_id"');
      }
    }
  };
}
