import io from 'socket.io-client';
import { EventSubscriber } from './EventSubscriber';
import * as utils from '../utils';
import {Client, Guild, GuildMember, Role} from "discord.js";
import {EventsMap} from 'socket.io-client/build/typed-events';

interface RautemusikOnair {
  [index: string]: string
}

export class Rautemusik extends EventSubscriber {
  private onair: RautemusikOnair = {};

  constructor(client: Client, config: Config) {
    super(client, config);

    if (false === !!Object.assign({}, config.rautemusik).enabled) {
      return;
    }

    this.eventListeners = {
      ready: this.readyEventListener
    };
  }

  takeOffAir = async (guild: Guild, onairRole: Role, discordId: string, stream: string) => {
    const member: GuildMember = await guild.members.fetch(discordId);

    console.log(`module.${this.constructor.name}: setting ${member.displayName} offair`);

    delete this.onair[stream];
    await member.roles.remove(onairRole);
    await utils.writeDM(member, `Ich habe dich bei **RauteMusik ${stream.toUpperCase()}** offair genommen.`);
  };

  eventHandler = async (eventObj: EventsMap) => {
    try {
      console.log(`module.${this.constructor.name}:`, eventObj);

      const guild     = await utils.findGuild(this.client, this.config.guild_id);
      const onairRole = await utils.findRole(guild, this.config.rautemusik.onair_role_id);

      // show end event from websocket
      if (!eventObj.data) {
        const stream = eventObj.stream;
        const discordId = this.onair[stream];

        if (discordId) {
          await this.takeOffAir(guild, onairRole, discordId, stream);
        }

        return;
      }

      const discordId = this.config.rautemusik.djs[eventObj.data.moderator.username];

      // if someone is onair and the discordId of the new moderate is different then the on onair previously => remove curent onair dj
      if (this.onair[eventObj.stream] && this.onair[eventObj.stream] !== discordId) {
        await this.takeOffAir(guild, onairRole, this.onair[eventObj.stream], eventObj.stream);
      }

      // moderator is not in the discord mapping
      if (!discordId) {
        return;
      }

      this.onair[eventObj.stream] = discordId;

      const member = await guild.members.fetch(discordId);

      if (!utils.hasRole(member, onairRole)) {
        console.log(`module.${this.constructor.name}: setting ${member.displayName} onair`);

        await member.roles.add(onairRole);
        await utils.writeOnairMessage(
          this.client,
          this.config.guild_id,
          this.config.rautemusik.announce_channels,
          `${member} ist jetzt auf dem **RauteMusik ${eventObj.stream.toUpperCase()}** Stream onair: https://www.rm.fm/${eventObj.stream}`
        );
      }
    } catch (error) {
      console.error(error);
    }
  };

  readyEventListener = async () => {
    try {
      for (const stream of this.config.rautemusik.streams) {
        const rmSocket = io('https://ws-api.rautemusik.fm');
        rmSocket.emit('subscribe', {stream: stream, events: ['show']});
        rmSocket.on('event', this.eventHandler);
      }
    } catch (error) {
      console.error(error);
    }
  };
}
