import { Client } from "discord.js";

interface EventListeners {
  [index: string]: (...args: any[]) => void;
}

export class EventSubscriber {
  protected client: Client;
  protected config: Config;
  protected eventListeners: EventListeners;

  constructor(client: Client, config: Config) {
    this.client         = client;
    this.config         = config;
    this.eventListeners = {};
  }

  subscribe = () => {
    const eventCount: number = Object.keys(this.eventListeners).length;
    let index: number        = 1;

    for (const [eventName, eventCallback] of Object.entries(this.eventListeners)) {
      this.client.on(eventName, eventCallback);

      console.log(`event.${this.constructor.name} (${index}/${eventCount}): registered ${eventName}`);

      index += 1;
    }
  };
}
