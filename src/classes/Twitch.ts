import { EventSubscriber } from './EventSubscriber';
import * as utils from '../utils';
import {Activity, Client, Guild, GuildMember, Presence, Role} from "discord.js";

export class Twitch extends EventSubscriber {
  constructor(client: Client, config: Config) {
    super(client, config);

    if (false === !!Object.assign({}, config.twitch).enabled) {
      return;
    }

    this.eventListeners = {
      presenceUpdate: this.presenceUpdateEventListener
    };
  }

  isActiveOnTwitch = (presence: Presence): Activity => {
    return presence.activities.find(activity => {
      return activity.name.toLowerCase() === 'twitch' && activity.type.toLowerCase() === 'streaming';
    });
  };

  presenceUpdateEventListener = async (oldPresence: Presence, newPresence: Presence) => {
    try {
      const guild: Guild        = await utils.findGuild(this.client, this.config.guild_id);
      const onairRole: Role     = await utils.findRole(guild, this.config.twitch.onair_role_id);
      const teamRole: Role      = await utils.findRole(guild, this.config.twitch.team_role_id);
      const member: GuildMember = newPresence.member;

      // member doesn't have the team role
      if (!utils.hasRole(member, teamRole)) {
        return;
      }

      const newTwitchActivity: Activity = this.isActiveOnTwitch(newPresence);

      // user was streaming on twitch before but not anymore
      if (oldPresence && this.isActiveOnTwitch(oldPresence) && !newTwitchActivity) {
        console.log(`event.${this.constructor.name}: setting ${member.displayName} offair`);
        await member.roles.remove(onairRole);
        await utils.writeDM(member, 'Ich habe dich bei **Twitch** offair genommen.');
        return;
      }

      // user now started streaming on twitch and doesn't have the onairRole yet
      if (newTwitchActivity && !utils.hasRole(member, onairRole)) {
        console.log(`event.${this.constructor.name}: setting ${member.displayName} onair`);
        await member.roles.add(onairRole);
        await utils.writeOnairMessage(
          this.client,
          this.config.guild_id,
          this.config.twitch.announce_channels,
          `${member} ist jetzt auf **Twitch** onair: ${newTwitchActivity.url}`
        );
      }
    } catch(error) {
      console.error(error);
    }
  };
}
