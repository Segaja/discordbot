import { EventSubscriber } from './EventSubscriber';
import * as fs from 'fs';
import * as path from 'path';
import {Client, Message} from "discord.js";

interface Command {
  callback: (client: Client, message: Message, parameters: string[]) => boolean;
  description: string;
  enabled: boolean;
  parameters: string[];
  callable_in_directmessage: boolean;
}

interface Commands {
  [index: string]: Command;
}

export class Messages extends EventSubscriber {
  private commands: Commands = null;

  constructor(client: Client, config: Config) {
    super(client, config);

    if (false === !!Object.assign({}, config.message).enabled) {
      return;
    }

    this.eventListeners = {
      message: this.messageEventListener
    };
  }

  loadCommands = async () => {
    if (false === !!Object.assign({}, this.config.message).enabled) {
      return;
    }

    if (null === this.commands) {
      this.commands = {};

      const commandFiles: string[] = fs.readdirSync(['.', 'src', 'commands'].join(path.sep));
      const commandCount: number = commandFiles.length;

      for (const [index, commandFile] of commandFiles.entries()) {
        const commandName: string = path.basename(commandFile, path.extname(commandFile));
        const command             = await import(['..', 'commands', commandFile].join(path.sep));

        if (!command.enabled) {
          continue;
        }

        this.commands[commandName] = command;

        console.log(`event.message.command (${index + 1}/${commandCount}): registered ${this.config.message.command_prefix}${commandName}`);
      }
    }
  };

  messageEventListener = async (message: Message) => {
    try {
      // commands have to start with "<commandPrefix>"
      if (!message.content.startsWith(this.config.message.command_prefix)) {
        return;
      }

      // command was send in a text channel but the channel is not registered in the config
      if ('text' === message.channel.type && !this.config.message.channels.includes(message.channel.id)) {
        return;
      }

      const parameters: string[] = message.content.split(/\s+/g).map(parameter => parameter.toLowerCase());
      const command: string      = parameters[0].substring(this.config.message.command_prefix.length).toLowerCase();

      // `<commandPrefix>help` command
      if ('help' === command) {
        let response: string = 'Available Commands:\n';

        for (const [key, command] of Object.entries(this.commands)) {
          const parameters: string[] = command.parameters;

          response += `**\`${this.config.message.command_prefix}${key}${parameters.length ? ' ' : ''}${command.parameters.join(' ')}\`**`;

          if (!command.callable_in_directmessage) {
            response += ' (not callable in DM)';
          }

          response += ` - ${command.description}\n`;
        }

        await message.reply(response);
        return;
      }

      // command not found
      if (!this.commands[command]) {
        await message.reply(`the command **\`${this.config.message.command_prefix}${command}\`** doesn't exist.`);
        return;
      }

      const commandObject = this.commands[command];

      if ('dm' === message.channel.type && !commandObject.callable_in_directmessage) {
        await message.reply(`the command **\`${this.config.message.command_prefix}${command}\`** is not callable in DM. Please call it in the appropriate channel.`);
        return;
      }

      // handle regular command
      await message.react('\u23F3'); // :hourglas_flowing_sand:
      const result = await commandObject.callback(this.client, message, parameters);

      if (result) {
        await message.react('\u2705'); // :white_check_mark:
      } else {
        await message.react('🛑'); // :stop_sign:
      }
    } catch (error) {
      console.error(error);
    }
  };
}
