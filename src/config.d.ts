interface Config {
    fan_users?: FanUsers;
    guild_id: string;
    message?: Message;
    rautemusik?: Rautemusik;
    twitch?: Twitch;
    weareone?: Weareone;
}

interface FanUsers {
    enabled: boolean;
    prefix: string;
    role_id: string;
}

interface Message {
    channels: string[];
    command_prefix: string;
    enabled: boolean;
}

interface Rautemusik {
    announce_channels: string[];
    enabled: boolean;
    djs: RauteMusikDJs;
    onair_role_id: string;
    streams: string[];
}

interface RauteMusikDJs {
    [index: string]: string;
}

interface Twitch {
    announce_channels: string[];
    enabled: boolean;
    onair_role_id: string;
    team_role_id: string;
}

interface Weareone {
    announce_channels: string[];
    dynamic_mapping?: WeareoneDynamicMapping;
    static_mapping?: WeareoneStaticMapping;
    enabled: boolean;
    onair_role_id: string;
}

interface WeareoneDynamicMapping {
    dj_group_id: string;
}

interface WeareoneStaticMapping {
    djs: WeareoneDJ[];
}

interface WeareoneDJ {
    announce: boolean;
    discordId: string;
    waoId: string;
}
