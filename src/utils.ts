import {
  Client,
  Collection,
  Guild,
  GuildChannel,
  GuildMember,
  Role,
  Snowflake,
  SnowflakeUtil,
  TextChannel,
  User
} from "discord.js";

interface GuildCache {
  [index: string]: Guild;
}

interface RoleCache {
  [index: string]: Role;
}

interface RoleNamePrefixCache {
  [index: string]: Collection<Snowflake, Role>;
}

let guildCache: GuildCache = {};
let roleCache: RoleCache = {};
let roleNamePrefixCache: RoleNamePrefixCache = {};

export const findGuild = async (client: Client, guildId: string): Promise<Guild> => {
  if (!guildCache[guildId]) {
    guildCache[guildId] = await client.guilds.fetch(guildId);
  }

  return guildCache[guildId];
};

export const findRole = async (guild: Guild, roleId: string): Promise<Role> => {
  if (!roleCache[roleId]) {
    roleCache[roleId] = await guild.roles.fetch(roleId);
  }

  return roleCache[roleId];
};

export const findRoleByName = async (guild: Guild, roleName: string): Promise<Role> => {
  if (!roleCache[roleName]) {
    const role: Role = await guild.roles.cache.find(role => role.name === roleName);

    if (!role) {
      return null;
    }

    roleCache[roleName] = role;
  }

  return roleCache[roleName];
};

export const findRolesByNamePrefix = async (guild: Guild, roleNamePrefix: string): Promise<Collection<Snowflake, Role>> => {
  if (!roleCache[roleNamePrefix]) {
    const roles : Collection<Snowflake,Role> = await guild.roles.cache.filter(role => role.name.startsWith(roleNamePrefix));

    if (!roles) {
      return null;
    }

    roleNamePrefixCache[roleNamePrefix] = roles;
  }

  return roleNamePrefixCache[roleNamePrefix];
};

export const hasRole = (member: GuildMember, role: Role): boolean => {
  return member.roles.cache.has(role.id);
}

export const writeDM = async (user: GuildMember, message: string) => {
  const dmChannel = await user.createDM();

  await dmChannel.send(message);
};

export const writeOnairMessage = async (client: Client, guildId: string, channels: string[], message: string) => {
  const guild = await findGuild(client, guildId);

  for (const channelId of channels) {
    const channel: GuildChannel = guild.channels.cache.get(channelId);
    await (channel as TextChannel).send(message);
  }
};
