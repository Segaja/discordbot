import * as Discord from 'discord.js';
import { ConfigLoader } from './classes/ConfigLoader';
import { FanUsers } from './classes/FanUsers';
import { Messages } from './classes/Messages';
import { Rautemusik } from './classes/Rautemusik';
import { Twitch } from './classes/Twitch';
import { WeAreOne } from './classes/WeAreOne';

const run = async () => {
  const configLoader: ConfigLoader = new ConfigLoader();
  const config = configLoader.get();

  const client: Discord.Client = new Discord.Client();

  const messages: Messages = new Messages(client, config);
  await messages.loadCommands();
  messages.subscribe();

  new FanUsers(client, config).subscribe();
  new Rautemusik(client, config).subscribe();
  new Twitch(client, config).subscribe();

  const weareone: WeAreOne = new WeAreOne(client, config);

  client.on('ready', async () => {
    console.log(`event.ready: connected as ${client.user.tag}`);

    weareone.init();
  });

  try {
    await client.login(process.env.bottoken);
  } catch (error) {
    console.error('error logging into discord. did you provide the environment variable "bottoken"?');
    console.error(error);
  }
};

setImmediate(run);
