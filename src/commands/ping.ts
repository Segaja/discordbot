import {Client, Message} from "discord.js";

export const callback = async (client: Client, message: Message, parameters: string[]) => {
  try {
    await message.reply(`pong - ${client.ws.ping} ms`);

    return true;
  } catch (error) {
    return false;
  }
};

export const description: string = 'Command to see if the bot is working and how fast it responds';

export const enabled: boolean = true;

export const parameters: string[] = [];

export const callable_in_directmessage: boolean = true;
