FROM node:16

COPY ./built/ /app/
COPY ./node_modules/ /app/node_modules/

WORKDIR /app

CMD ["node", "./src/bot.js"]
