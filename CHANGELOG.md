# Changelog

## 3.1.2
- don't load commands if the messages module is disabled

## 3.1.1
- re-add `node_modules` build dependencies to docker builds

## 3.1.0
- WeAreOne: assign dynamic "<radio>-onair" role to DJs who go onair

## 3.0.0
- migrate source code to TypeScript
- rework tasks (weareone) to proper class objects
- Rautemusik: fix typo in config key from `rautemuisk` to `rautemusik`

## 2.0.0
- WeAreOne: it is now possible to define if a dj should be announced in the `announce_channels` when he/she goes onair.
  for this to work the `weareone.djs` configuration value is now an array of DJ objects.
  Please check [the updated README.md](README.md#weareone-technobase) to see an explanation of the new structure.

## 1.1.1
- Rautemusik: finally fix when moderators go offair

## 1.1.0
- Rautemusik: fix another bug when setting moderators offair
- discordbot#1: messages.commands.ping: change command description to English
- discordbot#2: events.message: add `:hourglas_flowing_sand:` when the bot starts to process a command and either `:white_check_mark:` on success or `:stop_sign:` on failure once the bot finished executing the command
- discordbot#3: make all commands consistent to reply in channel, as they are also called as message commands from a channel
- discordbot#4: enable commands to be called from direct message to the bot, if the commands allows it

## 1.0.3
- Rautemusik: handle moderator changes between successive shows properly
- Bugfix: handle non-existent config blocks properly

## 1.0.2
- Rautemusik: Remove DJ from internal onair list when he is set offline
- Change Dockerfile to use `node bot.js` instead of `npm start` to avoid subshell

## 1.0.1
- Fix typo in the Rautemusik module.
- Add code navigation completion for Gitlab.

## 1.0.0
- initial public release. See [README.md](README.md) on how to use it.
