# Segaja's Discord Bot

This is a bot I wrote for an online radio community to deal with DJs being onair and announcing that to the community.
After that particular online radio closed down, I was asked if it was possible to integrate other platforms into the bot
to show that people were onair. The result is this bot with posible integrations to [Twitch](https://www.twitch.tv/),
[RauteMusik](https://www.rautemusik.fm/) and [TechnoBase.FM](https://www.technobase.fm/).
The bot is designed to be easily extendable with more event hooks and commands.

The bot is written in [Node.js](https://nodejs.org/) and based on [discord.js](https://discord.js.org/) and was designed
from the beginning to be run as a [Docker](https://www.docker.com/) image.

[[_TOC_]]

## Howto run the bot
There are docker images available in the [Container registry](https://gitlab.com/Segaja/discordbot/container_registry)
of this repository. Also the [distribute/docker-compose.yml](distribute/docker-compose.yml) was written for the sole
purpose of using these images and running the bot on your server.

For this to work you need to provide two things in the same folder as the `distribute/docker-compose.yml` is located:
- a `secrets.env` file that should have these values:
  ```dotenv
  bottoken=
  ```
  This is the token you get after [Creating a Bot Account](https://discordpy.readthedocs.io/en/latest/discord.html).
- a `config.json` which holds the information about the guild your bot works in and the configuration for the modules.
  Keep reading to get an idea of what this config file should look like.

  An example structure for this config file is provided in [distribute/config.json](distribute/config.json).

## The `config.json` file
[distribute/conifg.json](distribute/config.json) is the bare sceleton with the required values which are needed to run this bot.

- The `guild_id` needs to be a `string` with the ID of your discord guild / server.

See [Where can I find my User/Server/Message ID?](https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-)
to find out how to get IDs of objects from discord.

There are more config values which are all optional and will be explained in the documentation of each module.

## Modules
The bot has several modules and each of them can be enabled and disabled with a feature flag in the config file.
All modules are disabled by default.

### FanUsers
This is a module that assigns the member in your guild a special fan role if they have a defined prefix in front of their name.

The configuration of this module is done with the `fan_users` block in the `config.json` file. If this module is enabled via
the `fan_users.enabled` flag, then the following configuration block is required to exist:

```json
  "fan_users": {
    "enabled": true,
    "prefix": "yourAwesomePrefix|",
    "role_id": "0815"
  }
```
- The `enabled` flag defines if the module should be active or not. If it is set to `true` then the other configuration values also need to exist.
- The `prefix` is a string value that will be matched against the beginning of each members `displayName`. If the prefix is present at the beginning of the members `displayName` than the fan role will be assigned.
- The `role_id` defines which role to assign to the member in question if his `displayName` starts with the `prefix`.

### Message commands
This module allowes you to define certain commands that users can enter in a specific channel and the bot then executes.

The configuration of this module is done with the `message` block in the `config.json` file. If this module is enabled via
the `message.enabled` flag, then the following configuration block is required to exist:

```json
  "message": {
    "channels": [
      "1234",
      ...
    ],
    "command_prefix": "<commandPrefix>",
    "enabled": true
  },
```
- The `enabled` flag defines if the module should be active or not. If it is set to `true` then the other configuration values also need to exist.
- `channels` is a list of channels the module listens to. This means only messages within one of these channels is tried to be evaluated as a command.
- `command_prefix` is the prefix a message must have to be considered a command (e.g. `!`).

For now only two commands exist and will be enabled by default if this module is being activated:
- `<commandPrefix>help`: a command that sends a DirectMessage to the author of the command and gives him an overview of all available commands
- `<commandPrefix>ping`: a command that sends a DirectMessage to the author of the command that contains of the message "pong" and the time it took for the bot to respond to the command

### RauteMusik
This module is using the [RauteMusik](https://www.rm.fm/) Websocket API to be notified when a DJ goes onair on one of the defined Rautemusik streams.

The configuration of this module is done with the `rautemusik` block in the `config.json` file. If this module is enabled via
the `rautemusik.enabled` flag, then the following configuration block is required to exist:

```json
  "rautemusik": {
    "announce_channels": [
      "1234",
      ...
    ],
    "enabled": true,
    "djs": {
      "<username_of_dj_on_rautemusik_website>": "<discord_user_id_of_this_dj>",
      ...
    },
    "onair_role_id": "0815",
    "streams": [
      "club",
      ...
    ]
  }
```
- The `enabled` flag defines if the module should be active or not. If it is set to `true` then the other configuration values also need to exist.
- `announce_channels` is an array with IDs for channels to which to write the message that the member is now onair on Rautemusik together with the link to the webplayer of Rautemusik. Currently this message is only in German.
- `djs` is a mapping between the username of the DJ on the Rautemusik website and the corresponding memberId of this DJ on discord.
- `onair_role_id` defines the role which the member gets assigned to if he is onair on Rautemusik.
- `streams` is an array that defines for which RauteMusik stream to enable this module.

### Twitch
This module monitors all member in a certain group to see if they recently got onair at [Twitch](https://www.twitch.tv/).
This requires the member to have the [Twitch Integration](https://support.discord.com/hc/en-us/articles/212112068-Twitch-Integration-FAQ)
setup for their account and to be [visible](https://support.discord.com/hc/en-us/articles/227779547-Changing-Online-Status).

The configuratio of this module is done with the `twitch` block in the `config.json` file. If this module is enabled via
the `twitch.enabled` flag, then the following configuration block is required to exist:

```json
  "twitch": {
    "announce_channels": [
      "1234",
      ...
    ],
    "enabled": true,
    "onair_role_id": "0815",
    "team_role_id": "9876"
  }
```
- The `enabled` flag defines if the module should be active or not. If it is set to `true` then the other configuration values also need to exist.
- `announce_channels` is an array with IDs for channels to which to write the message that the member is now onair on Twitch together with the link to his twitch profile. Currently this message is only in German.
- `onair_role_id` defines the role which the member gets assigned to if he is onair on Twitch.
- `team_role_id` is the role to define which members should be considered as "DJ" for this module. Only members in this role will be watched for twitch activity.

### WeAreOne (TechnoBase)
This module is checking every minute the [Technobase](https://www.technobase.fm/) API to see if a dj has gone onair on any of its streams.

The configuratio of this module is done with the `weareone` block in the `config.json` file. If this module is enabled via
the `weareone.enabled` flag, then the following configuration block is required to exist:

```json
  "weareone": {
    "announce_channels": [
      "1234"',
      ...
    ],
    "static_mapping": {
      "djs": [
        {
          "announce": true,
          "discordId": "<discord_user_id_of_this_dj>",
          "waoId": "<userId_of_dj_on_technobase_website>"
        },
        ...
      ]
    },
    "dynamic_mapping": {
      "dj_group_id": "<discord_role_id_for_dj_group>"
    },
    "enabled": true,
    "onair_role_id": "0815"
  }
```
- The `enabled` flag defines if the module should be active or not. If it is set to `true` then the other configuration values also need to exist.
- `announce_channels` is an array with IDs for channels to which to write the message that the member is now onair on Technobase together with the link to the webplayer of Rautemusik. Currently this message is only in German.
- `static_mapping` defines a static mapping between TechnoBase DJ IDs and discord IDs (either this or `dynamic_mapping` has to be set)
  - `djs` is an array of DJs which should be considered by the module.
    - `announce` defines if a message should be written in the `announce_channels` if this DJ goes onair.
    - `discordId` is the discord user ID this DJ has. This is needed to map between the TechnoBase user and his/her discord account.
    - `waoId` is the ID of the DJ on the TechnoBase homepage. The module will look in the schedule of all streams of TechnoBase for this ID.
- `dynamic_mapping` will match the name of the DJ from TechnoBase directly against the djs by name in the discord. For this the DJs need to have on discord the same name as on TechnoBase (either this or `static_mapping` has to be set)
  - `dj_group_id` the discord role that all DJs need to have to be enlisted to set onair
- `onair_role_id` defines the role which the member gets assigned to if he is onair on TechnoBase.

If you have a role called `onair-<radioname>`, the bot will also assign that role to any dj that goes onair for that WAO radio.

## Local development
It is possible to run the bot directly from the code base of this repository. For this you need to run the following
commands inside of this repositories root folder:
```shell
docker-compose run npm
docker-compose up discordbot
```
This will install the needed [npm](https://www.npmjs.com/) (listed in the [package.json](package.json)) dependencies
and start the bot.

The same rules about the `config.json` and `secrets.env` file that need to be provided apply as if you would want to run
it on a release version (see [How to run the bot](#howto-run-the-bot)).

## Thanks

My thanks go out to [TechnoBase.FM](https://www.technobase.fm) and [RauteMusik.FM](https://www.rautemsuik.fm) for allowing
me to publish the code that interacts with their API. Furthermore I thank all the DJs in the old [TECHNO4EVER discord server](https://discord.gg/erbxbhd)
for helping me to test the bot and figure out some annoying bugs.

Special Thanks go to @T4cC0re who helped me to understand Node.JS. Thanks for being so patient with me.
